package com.booking.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
  // workStage (In Process, Finish, Canceled)

public class Reservation {
    private static int generateId = 1;
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;

    @Builder
    public Reservation(Customer customer, Employee employee, List<Service> services, String workstage) {
        this.reservationId = generateReservationId();
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
    }

    private synchronized String generateReservationId() {
        String generateReservationId = "Rsv-" + String.format("%02d", generateId);
        generateId++;
        return generateReservationId;
    }

    private double calculateReservationPrice() {
        double totalPrice = 0;
        
        if (customer != null && customer.getMember() != null && services != null && !services.isEmpty()) {
            Service service = services.get(0);  

            if ("Silver".equalsIgnoreCase(customer.getMember().getMembershipName())) {
                totalPrice =service.getPrice()  - (service.getPrice() * 0.05);
            } else if ("Gold".equalsIgnoreCase(customer.getMember().getMembershipName())) {
                totalPrice = service.getPrice()  - (service.getPrice() * 0.10);
            } else if ("none".equalsIgnoreCase(customer.getMember().getMembershipName())) {
                totalPrice = service.getPrice();
            }
        }

        return totalPrice;
    }
}
