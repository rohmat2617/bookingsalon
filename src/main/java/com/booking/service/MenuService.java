package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Reservasi;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    public static List<Reservasi> personList = PersonRepository.getAllPerson();

    public static List<Service> serviceList = ServiceRepository.getAllService();
    public static List<Reservation> reservationList = new ArrayList<>();

    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = { "Show Data", "Create Reservation", "Complete/cancel reservation", "Exit" };
        String[] subMenuArr = { "Recent Reservation", "Show Customer", "Show Available Employee", "Back to main menu" };

        int optionMainMenu;
        int optionSubMenu;

        boolean backToMainMenu = false;
        boolean backToSubMenu = false;

        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = Integer.valueOf(input.nextLine());
            switch (optionMainMenu) {
                case 1:
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = Integer.valueOf(input.nextLine());
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                PrintService.showRecentReservation(reservationList);
                                break;
                            case 2:
                                PrintService.showAllCustomer(personList);
                                break;
                            case 3:
                                PrintService.showAllEmployee(personList);
                                break;
                            case 4:
                                // panggil fitur tampilkan history reservation + total keuntungan
                                break;
                            case 0:
                                backToSubMenu = true;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                    String inputCustomerId = "";
                    String inputEmployeeId = "";
                    String inputServiceId = "";

                    do {
                        PrintService.showAllCustomer(personList);
                        inputCustomerId = ReservationService.getValidCustomerId(input);
                    } while (ReservationService.getValidCustomerByListId(personList, inputCustomerId) == null);

                    do {
                        PrintService.showAllEmployee(personList);
                        inputEmployeeId = ReservationService.getValidEmployeeId(input);
                    } while (ReservationService.getValidEmployeeByListId(personList,
                            inputEmployeeId) == null);

                    do {
                        PrintService.showServices(serviceList);
                        inputServiceId = ReservationService.getValidServisId(input, reservationList);
                    } while (ReservationService.getValidServisByListId(serviceList,
                            inputServiceId) == null);

                    ReservationService.ExecuteAddReservation(inputCustomerId, inputEmployeeId, inputServiceId);
                    System.out.println("sucsess");
                    break;
                case 3:
                    String inputReservasiId = "";
                    String inputComplete = "";
                    do {
                        PrintService.showFinishOrCancel(reservationList);
                        inputReservasiId = ReservationService.getValidReservationId(input);

                    } while (ReservationService.getValidReservationByListId(reservationList, inputReservasiId) == null);
                    inputComplete = ReservationService.getFinishOrCancel(input);
                    ReservationService.executeBackReservation(inputComplete, inputReservasiId, reservationList, personList);
                    System.out.println(inputComplete);
                    break;
                case 0:
                    backToMainMenu = false;
                    break;
            }
        } while (!backToMainMenu);

    }

    

}
