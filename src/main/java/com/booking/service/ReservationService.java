package com.booking.service;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservasi;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    public static final String getCustomerByCustomerId = null;

    public static void createReservation() {

    }

    public static String getValidCustomerId(Scanner input) {
        String inputCustomerId;
        boolean isValid;
        do {
            System.out.print("Input The Customer Id: ");
            inputCustomerId = input.nextLine();
            isValid = !inputCustomerId.trim().isEmpty();
            if (!isValid) {
                System.out.println("Incorrect Name Filling. Make Sure It Only Consists Of Letters And Is Not Empty.");
            }
        } while (!isValid);
        return inputCustomerId;
    }

    public static Reservasi getValidCustomerByListId(List<Reservasi> listPersons, String inputCustomerId) {
        Optional<Reservasi> optionalCustomerId = findCustomerById(listPersons, inputCustomerId);
        if (optionalCustomerId.isPresent()) {
            Reservasi customerid = optionalCustomerId.get();
            System.out.println("Id Found, The ID You Entered Is " + customerid.getId());

            return customerid;
        } else {
            System.out.println("Employee With ID " + inputCustomerId + " Not Found.");
            return null;
        }
    }

    public static Optional<Reservasi> findCustomerById(List<Reservasi> listPersons, String inputCustomerId) {
        return listPersons.stream().filter(customer -> customer.getId().equalsIgnoreCase(inputCustomerId))
                .findFirst();
    }

    public static String getValidEmployeeId(Scanner input) {
        String inputEmployeeId;
        boolean isValid;
        do {
            System.out.print("Input The Employee Id: ");
            inputEmployeeId = input.nextLine();
            isValid = !inputEmployeeId.trim().isEmpty();
            if (!isValid) {
                System.out.println("Incorrect Name Filling. Make Sure It Only Consists Of Letters And Is Not Empty.");
            }
        } while (!isValid);
        return inputEmployeeId;
    }

    public static Reservasi getValidEmployeeByListId(List<Reservasi> listPersons, String inputEmployeeId) {
        Optional<Reservasi> optionalEmployeeId = findCustomerById(listPersons, inputEmployeeId);
        if (optionalEmployeeId.isPresent()) {
            Reservasi employeeid = optionalEmployeeId.get();
            System.out.println("Id Found, The ID You Entered Is " + employeeid.getId());

            return employeeid;
        } else {
            System.out.println("Employee With ID " + inputEmployeeId + " Not Found.");
            return null;
        }
    }

    public static Optional<Reservasi> findEmployeeById(List<Reservasi> listPersons, String inputEmployeeId) {
        return listPersons.stream().filter(employee -> employee.getId().equalsIgnoreCase(inputEmployeeId))
                .findFirst();
    }

    public static String getValidServisId(Scanner input, List<Reservation> reservationList) {
        String inputServiceId;
        boolean isValid;
        boolean shouldRepeat = true;
        do {
            System.out.print("Input The Service Id: ");
            inputServiceId = input.nextLine();
            isValid = !inputServiceId.trim().isEmpty();

            if (isValid && isServiceIdAlreadyExists(inputServiceId, reservationList)) {
                System.out.println("Service Id already exists in a reservation. Please enter a different Service Id.");
                isValid = false;

            } else if (!isValid) {
                System.out.println("Incorrect Name Filling. Make Sure It Only Consists Of Letters And Is Not Empty.");
            } else {
                System.out.print("Do you want to enter another Service Id? (Y/T): ");
                String repeatInput = input.nextLine().toLowerCase();
                shouldRepeat = repeatInput.equals("Y");
            }
        } while (!isValid || shouldRepeat);
        return inputServiceId;
    }

    private static boolean isServiceIdAlreadyExists(String inputServiceId, List<Reservation> reservationList) {
        return reservationList.stream()
                .anyMatch(reservation -> reservation.getServices()
                        .stream()
                        .anyMatch(service -> service.getServiceId().equalsIgnoreCase(inputServiceId)));
    }

    public static Service getValidServisByListId(List<Service> serviceList, String inputServiceId) {
        Optional<Service> optionalServiceId = findServiceById(serviceList, inputServiceId);
        if (optionalServiceId.isPresent()) {
            Service serviceid = optionalServiceId.get();
            System.out.println("Id Found, The ID You Entered Is " + serviceid.getServiceId());
            return serviceid;
        } else {
            System.out.println("Service With ID " + inputServiceId + " Not Found.");
            return null;
        }
    }

    public static Optional<Service> findServiceById(List<Service> serviceList, String inputServiceId) {
        return serviceList.stream().filter(employee -> employee.getServiceId().equalsIgnoreCase(inputServiceId))
                .findFirst();
    }

    public static String getValidReservationId(Scanner input) {
        String inputReservasiId;
        boolean isValid;

        do {
            System.out.print("Input The Reservasi Id: ");
            inputReservasiId = input.nextLine();
            isValid = !inputReservasiId.trim().isEmpty();
            if (!isValid) {
                System.out.println("Incorrect Name Filling. Make Sure It Only Consists Of Letters And Is Not Empty.");
            }
        } while (!isValid);
        return inputReservasiId;
    }

    public static Reservation getValidReservationByListId(List<Reservation> reservationList, String inputReservasiId) {
        Optional<Reservation> optionalReservation = findReservationById(reservationList, inputReservasiId);
        if (optionalReservation.isPresent()) {
            Reservation reservation = optionalReservation.get();
            System.out.println("Reservation Found, The ID You Entered Is " + reservation.getReservationId());
            return reservation;
        } else {
            System.out.println("Reservation With ID " + inputReservasiId + " Not Found.");
            return null;
        }
    }

    public static Optional<Reservation> findReservationById(List<Reservation> reservationList,
            String inputReservasiId) {
        return reservationList.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(inputReservasiId))
                .findFirst();
    }

    public static String getFinishOrCancel(Scanner input) {
        String complete;
        boolean isValid;

        do {
            System.out.print("Do you want to complete? (Finish/Cancel): ");
            complete = input.nextLine().trim();
            isValid = (complete.equalsIgnoreCase("Finish") || complete.equalsIgnoreCase("Cancel"))
                    && !complete.isEmpty();

            if (!isValid) {
                System.out.println("Incorrect input. Make sure it is either 'Finish' or 'Cancel'.");
            }
        } while (!isValid);

        return complete;
    }

    public static void ExecuteAddReservation(String inputCustomerId, String inputEmployeeId, String inputServiceId) {
        Reservasi customerPerson = null;
        Reservasi employeePerson = null;
        Service service = null;

        Iterator<Reservasi> iterator = MenuService.personList.iterator();
        while (iterator.hasNext()) {
            Reservasi person = iterator.next();
            if (person instanceof Customer && person.getId().equals(inputCustomerId)) {
                customerPerson = person;
            }
            if (person instanceof Employee && person.getId().equals(inputEmployeeId)) {
                iterator.remove();
                employeePerson = person;
            }

        }

        for (Service serviceItem :  MenuService.serviceList) {
            if (serviceItem.getServiceId().equals(inputServiceId)) {
                service = serviceItem;
                break;
            }
        }

        Reservation newReservation = Reservation.builder()
                .customer((Customer) customerPerson)
                .employee((Employee) employeePerson)
                .services(Arrays.asList(service))
                .workstage("In process")
                .build();

         MenuService.reservationList.add(newReservation);

    }

    public static void executeBackReservation(String inputComplete, String inputReservasiId,
            List<Reservation> reservationList, List<Reservasi> personList) {
        Optional<Reservation> removedReservationOptional = ReservationService.findReservationById(reservationList,
                inputReservasiId);

        if (removedReservationOptional.isPresent()) {
            Reservation reservation = removedReservationOptional.get();
            reservation.setWorkstage(inputComplete);

            if (inputComplete.equalsIgnoreCase("Finish")) {
                double newWallet = reservation.getCustomer().getWallet() - reservation.getReservationPrice();
                reservation.getCustomer().setWallet(newWallet);
            }

            if (shouldAddEmployeeBack(inputComplete)) {
                addEmployeeBackToPersonList(reservation.getEmployee(), personList);
            }

            System.out.println("Workspace Is Set To, " + reservation.getWorkstage());
        }
    }

    private static boolean shouldAddEmployeeBack(String inputComplete) {
        return inputComplete.equalsIgnoreCase("Finish");
    }

    private static void addEmployeeBackToPersonList(Employee employee, List<Reservasi> personList) {

        if (!personList.contains(employee)) {
            personList.add(employee);
        }
    }
    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
