package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservasi;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {

    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        StringBuilder result = new StringBuilder();

        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result.append(service.getServiceName()).append(", ");
        }

        if (result.length() > 0) {
            result.setLength(result.length() - 2);
        }

        return result.toString();
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Pegawai", "Workstage");
        System.out
                .println("+========================================================================================+");
        for (Reservation reservation : reservationList) {

            System.out.printf("| %-4s |", num);
            System.out.printf("| %-10s |", reservation.getReservationId());
            System.out.printf("| %-25s |", reservation.getCustomer().getName());
            System.out.printf("| %-15s |", printServices(reservation.getServices()));
            System.out.printf("| %-10s |", reservation.getReservationPrice());
            System.out.printf("| %-10s |", reservation.getEmployee().getName());
            System.out.printf("| %-10s |", reservation.getWorkstage());
            System.out.println();
            num++;
        }
    }

    public static void showAllCustomer(List<Reservasi> personList) {
        int num = 1;
        System.out.printf("| %-4s | %-6s | %-11s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out
                .println("+========================================================================================+");

        for (Reservasi employee : personList) {
            if (employee instanceof Customer) {
                Customer cos = (Customer) employee;
                System.out.printf("| %-4s | %-6s | %-11s | %-15s | %-15s | %-15s |\n",
                        num, cos.getId(), cos.getName(), cos.getAddress(), cos.getMember().getMembershipName(),
                        cos.getWallet());
                num++;
            }
        }
    }

    public static void showAllEmployee(List<Reservasi> personList) {
        int num = 1;
        System.out.printf("| %-4s | %-6s | %-11s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out
                .println("+========================================================================================+");

        for (Reservasi employee : personList) {
            if (employee instanceof Employee) {
                Employee emp = (Employee) employee;
                System.out.printf("| %-4s | %-6s | %-11s | %-15s | %-15s |\n",
                        num, emp.getId(), emp.getName(), emp.getAddress(), emp.getExperience());
                num++;
            }
        }
    }

    public static void showServices(List<Service> serviceList) {
        int num = 1;
        System.out.printf("| %-4s | %-9s | %-25s | %-15s |\n",
                "No.", "ID", "Jenis Service", "Harga");
        System.out
                .println("+========================================================================================+");

        for (Service servis : serviceList) {

            Service serv = (Service) servis;
            System.out.printf("| %-4s | %-9s | %-25s | %-15s |\n",
                    num, serv.getServiceId(), serv.getServiceName(), serv.getPrice());
            num++;

        }
    }

    public static void showFinishOrCancel(List<Reservation> reservationList) {
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Pegawai", "Workstage");
        System.out
                .println("+========================================================================================+");
        for (Reservation reservation : reservationList) {

            System.out.printf("| %-4s |", num);
            System.out.printf("| %-10s |", reservation.getReservationId());
            System.out.printf("| %-25s |", reservation.getCustomer().getName());
            System.out.printf("| %-15s |", printServices(reservation.getServices()));
            System.out.printf("| %-10s |", reservation.getReservationPrice());
            System.out.println();
            num++;
        }
    }
    public static void showHistoryReservation() {

    }
}
